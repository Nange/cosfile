// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import babelpolyfill from 'babel-polyfill';
import Vue from 'vue';
import App from './App';
import jsonp from 'jsonp';
import router from './router';
import ElementUI from 'element-ui';
import axios from 'axios';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false;
Vue.use(ElementUI);

/*axios*/
axios.defaults.baseURL = process.env.API_ROOT;
axios.defaults.withCredentials = true;
Vue.prototype.$http = axios;
Vue.prototype.$jsonp = jsonp;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
});