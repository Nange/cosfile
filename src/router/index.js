import Vue from 'vue'
import Router from 'vue-router'
//首页
import index from '../views/index.vue'

Vue.use(Router)

let routes = [
	//首页
	{
		path:'/',
		name:'index',
		component:index
	}
]

const router = new Router({
  	routes,
  	mode:'history'
})

export default router;
